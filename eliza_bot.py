# TODO: implement random conversations
import telegram
from telegram.ext import Updater
import logging
from eliza import analyze
from datetime import datetime, timedelta
import json
from collections import defaultdict
import random
import sys
import sched
from threading import Thread, Event


class PesterThread(Thread):
    def __init__(self):
        super().__init__()
        self.terminate = Event()
        self.scheduler = sched.scheduler()

    def run(self):
        while not self.terminate.wait(timeout=1):
            self.scheduler.run(blocking=False)
        return


class ElizaBot(object):
    user_states = defaultdict(dict)

    def __init__(self, token: str, delay_minutes: int=0, delay_fuzz: int=0):
        self.logger = logging.getLogger("ElizaBot")
        self.token = token
        self.delay_minutes = delay_minutes
        self.delay_fuzz = delay_fuzz
        self.updater = Updater(token)
        self.dp = self.updater.dispatcher
        self.dp.addTelegramCommandHandler("start", self.start_therapy)
        self.dp.addTelegramCommandHandler("stop", self.stop)
        self.dp.addTelegramMessageHandler(self.message_handler)
        self.pester_thread = PesterThread()
        random.seed()
        self.logger.info("created")

    def update_status(self, bot: telegram.Bot, update: telegram.Update):
        user_id = update.message.from_user.id
        self.user_states[user_id]['last_chat_id'] = update.message.chat_id
        self.user_states[user_id]['last_spoke'] = datetime.now()
        # cancel any scheduled pestering
        event = self.user_states[user_id].get('event', None)
        if event:
            try:
                self.pester_thread.scheduler.cancel(event)
                self.logger.info("canceled pestering.")
            except ValueError:
                self.logger.info("no pestering to cancel.")
            self.user_states[user_id]['event'] = None
        if self.user_states[user_id]['in_session'] and self.delay_minutes > 0:
            # schedule the next pestering
            # randomize the delay a little
            fuzz = random.randint(0, self.delay_fuzz * 2) - delay_fuzz if delay_fuzz else 0
            delay_seconds = (self.delay_minutes + fuzz) * 60
            self.user_states[user_id]['event'] = self.pester_thread.scheduler.enter(
                delay_seconds, 0, lambda: self.pester(bot, update))
            self.logger.info("Scheduled a pestering for {} at {}.".format(
                update.message.from_user.username, datetime.now() + timedelta(seconds=delay_seconds)))

    def start_therapy(self, bot, update):
        self.logger.info("start: update = {}".format(str(update)))
        self.user_states[update.message.from_user.id]['in_session'] = True
        self.update_status(bot, update)
        self.pester(bot, update)

    def pester(self, bot, update):
        self.logger.info("pestering {}".format(update.message.from_user.username))
        bot.sendMessage(update.message.chat_id, "How are you feeling?")

    def message_handler(self, bot, update):
        self.logger.info("message: update = {}".format(str(update)))
        self.update_status(bot, update)
        bot.sendMessage(update.message.chat_id,
                        analyze(update.message.text))

    def stop(self, bot, update):
        self.logger.info("stop: update = {}".format(str(update)))
        self.user_states[update.message.from_user.id]['in_session'] = False
        self.update_status(bot, update)
        bot.sendMessage(update.message.chat_id,
                        "I'm glad we had this chat.")

    def run(self, background=False):
        if background:
            raise NotImplementedError("Background isn't implemented yet :-(")
        else:
            self.logger.info("starting the pester thread...")
            self.pester_thread.start()
            self.logger.info("starting")
            self.updater.start_polling()
            self.updater.idle()
            self.logger.info("signalling the pester thread...")
            self.pester_thread.terminate.set()
            self.logger.info("joining the pester thread...")
            self.pester_thread.join(5)
            if self.pester_thread.is_alive():
                self.logger.warn("join failed on pester thread?!?")


if __name__ == "__main__":
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                        level=logging.INFO)
    # get root logger so we can set global debug level
    logger = logging.getLogger()

    logger.info("Reading config...")
    with open('config.json', 'r') as f:
        j = json.loads(f.read())
    try:
        the_token = j['default']['token']
    except KeyError as e:
        logger.critical("No token set! Exiting.")
        sys.exit(1)
    if 'log_level' in j['default'].keys():
        logger.setLevel(j['default']['log_level'].upper())
    # default delay values
    delay_minutes = 60
    delay_fuzz = 0
    try:
        delay_minutes = int(j['default']['delay'])
        if delay_minutes < 0:
            delay_minutes = -delay_minutes
        try:
            delay_fuzz = int(float(j['default']['fuzz']))
            logger.info("Delay = {:d} ± {:d} minutes.".format(delay_minutes, delay_fuzz))
        except KeyError as e:
            logger.info("No fuzz set, no fuzzing enabled.")
        if delay_fuzz > delay_minutes or delay_fuzz < 0:
            delay_fuzz = 0
    except KeyError as e:
        logger.info("No delay set, defaulting to an hour.")

    logger.info("Creating ElizaBot...")
    the_bot = ElizaBot(the_token, delay_minutes, delay_fuzz)

    logger.info("Running ElizaBot. ^C to terminate. (And give it a sec.)")
    the_bot.run()
